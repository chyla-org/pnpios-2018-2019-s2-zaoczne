# Czas

Klasa Czas służąca do zapamiętania okresu czasu tj. liczby godzin i minut.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Running the tests

Running the tests:

```
python -m unittest discover
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
