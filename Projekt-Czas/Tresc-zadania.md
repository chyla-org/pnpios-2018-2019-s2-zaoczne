Czas
====

Stwórz nowy projekt i napisz klasę Czas służącą do zapamiętania okresu czasu tj. liczby godzin i minut.
Klasa ta powinna mieć dwa pola prywatne:
 * godz;
 * minuty;
oraz metody publiczne:
 * konstruktor z parametrami będącymi liczbą godzin i minut, gdy zostanie podany jeden parametr należy potraktować go jako łańcuch znaków na podstawie którego można ustalić wartość godzin i minut np. ”12 h 58 min” (funkcja split() pozwala dzielić napis według określonego separatora: https://www.w3schools.com/python/ref_string_split.asp)
 * to_string() której wynikiem jest łańcuch znaków opisujący dany okres czasu, np. ”29 h 19 min”
 * dodaj(t) której wynikiem jest nowy obiekt klasy Czas będący sumą
bieżącego i podanego jako parametr obiektu Czas (dodaj odpowiednie metody)
 * odejmij(t) analogicznie jak dodaj, tyle że odejmowanie,
 * pomnoz(ile) wynikiem ma być okres czasu pomnożony podaną liczbę razy,


Sprawdź zgodność z dokumentem PEP 8 używając programu pycodestyle.
